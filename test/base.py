import unittest
import sys
import os
sys.path.append(os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    "lib"
))


class TestCase(unittest.TestCase):
    maxDiff = None
